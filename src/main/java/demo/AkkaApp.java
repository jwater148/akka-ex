package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.OnSuccess;
import akka.pattern.Patterns;
import akka.util.Timeout;
import demo.actors.FileAnalysisActor;
import demo.messages.FileAnalysisMessage;
import demo.messages.FileProcessedMessage;
import demo.messages.Tweet;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class AkkaApp {


    public static class StaticPath {
        //public static String path = "lib/2020A_tweets/selected/r";
        public static String path = "data/tweets/smol";
        public static String output_file = "new_with_offset";
        public static List<Tweet> tweetList  = new ArrayList<>();
    }

    public static void main(String[] args) {

        // Get jsonl files
        try (Stream<Path> paths = Files.walk(Paths.get(StaticPath.path),2)) {
            paths.map(Path::toString).filter(f -> f.endsWith(".jsonl"))
                    .forEach(AkkaApp::parseEvent);
        } catch (Exception e) { e.printStackTrace(); }


    }

    private static void parseEvent(String s) {
        System.out.println("Parsing " + s);

        // Create actorSystem
        ActorSystem akkaSystem = ActorSystem.create("akkaSystem");


        // Create the first actor based on the specified class
        Props props = Props.create(FileAnalysisActor.class);
        ActorRef coordinator = akkaSystem.actorOf(props);


        // Create a message including the file path
        FileAnalysisMessage msg = new FileAnalysisMessage(s);


        // Send a message to start processing the file.
        // This is a synchronous call using 'ask' with a timeout.
        Timeout timeout = new Timeout(50, TimeUnit.SECONDS);
        Future<Object> future = Patterns.ask(coordinator, msg, timeout);


        // Process the results
        final ExecutionContext ec = akkaSystem.dispatcher();
        future.onSuccess(new OnSuccess<Object>() {

            @Override
            public void onSuccess(Object message) throws Throwable {

                if (message instanceof FileProcessedMessage) {

                    printResults((FileProcessedMessage) message);

                    akkaSystem.shutdown(); // Stop the actor system
                }
            }

            private void printResults(FileProcessedMessage message) {


                message.getHMap().forEach(outputs->{
                    outputs.getTweets().forEach(output->{
                        System.out.println(output.getCreatedAt() );
                                //+", id : "+ Arrays.toString(output.getDimensions()));

                    });

                });
            }
        }, ec);
    }
}
